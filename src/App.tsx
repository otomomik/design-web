import { RouterProvider, createBrowserRouter } from 'react-router-dom'

import { Editor } from '@/routes/Editor'
import { DesignerEditor } from '@/routes/Editor/Designer'
import { Home } from '@/routes/Home'

import type { FC } from 'react'

const router = createBrowserRouter([
  {
    path: '/',
    element: <Home />,
  },
  {
    path: '/editor',
    element: <Editor />,
    children: [
      {
        path: 'designer',
        element: <DesignerEditor />,
      },
    ],
  },
])

export const App: FC = () => {
  return <RouterProvider router={router} />
}
